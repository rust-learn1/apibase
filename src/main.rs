// Local crates
mod infrastructure;
mod application;
mod domain;

use std::io::Result;
use actix_web::{App, HttpServer, web};
use crate::infrastructure::api::api_definition;

#[actix_rt::main]
async fn main() -> Result<()>{
    let app_closure = || {
        App::new()
            .service(web::scope("/api").configure(api_definition))
    };

    let mut server = HttpServer::new(app_closure);
    server = match server.bind("127.0.0.1:8080") {
        Ok(s) => s,
        Err(e) => panic!("{}", e)
    };
    server.run().await
}
