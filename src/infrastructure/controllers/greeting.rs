use actix_web::{Responder, web};
use crate::application::say_hello;

pub async fn index () -> impl Responder{
    let new_greeting = say_hello::create_greeting();
    web::Json(new_greeting)
}