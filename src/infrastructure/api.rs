use actix_web::{web};
use super::controllers::greeting;

pub fn api_definition(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/")
            .route(web::get().to(greeting::index))
    );
}