use serde::{Serialize};

#[derive(Serialize)]
pub struct Greet {
    pub content: String
}